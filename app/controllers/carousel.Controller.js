const mongoose = require("mongoose");
//import Model
const carouselModel = require("../models/carousel.Model");


// create
const createCarousel = (req, res)=>{
    // b1: get data from request
    let body = req.body;
    
    // b2: validate
   
    if(!body.name){
        return res.status(400).json({
            status: "Bad Request",
            message : "name is required"
        })
    }
    
    if(!body.description){
        return res.status(400).json({
            status: "Bad Request",
            message : "description is required"
        })
    }
    if(!body.imageUrl){
        return res.status(400).json({
            status: "Bad Request",
            message : "imageUrl is required"
        })
    }
    
    // b3: xử lý và trả về dữ liệu
    var newCarousel = new carouselModel({
        _id:mongoose.Types.ObjectId(),
        name : body.name,
        description : body.description,
        imageUrl: body.imageUrl
    });

    carouselModel.create(newCarousel,(err,data) =>{
        if(err){
            return res.status(500).json({
                message : `Internal server error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message :`Create New carousel Successfully !`,
                carousel : data
            })
        }
       
        
    })

}
// get all carousel
const getAllCarousel = (req, res)=>{
    // b1: get data from request
   
    // b2: validate
    // b3: xử lý dữ liệu và trả về kết quả
    carouselModel.find((err, data)=>{
        if(err){
            return res.status(500).json({
                status: "internal server error",
                error: err.message
            })
        }else{
            return res.status(200).json({
                message: "get all carousel successfuly",
                carousel: data
            })
        }
    })
}

// get carousel by id
const getCarouselById = (req, res)=>{
    // b1: get data from request
    let carouselId = req.params.carouselId;
    // b2: validate
    if(!mongoose.Types.ObjectId.isValid(carouselId)){
        return res.status(400).json({
            message: "carouselId is not valid"
        })
    }
    
    // b3: xử lý và trả về dữ liệu
    
    carouselModel.findById(carouselId,(err,data) =>{
        if(err){
            return res.status(500).json({
                message : `Internal server error : ${err.message}`
            })
        }
        if(data){
            return res.status(200).json({
                message: "get carousel by id successfuly",
                productType: data
            })
        }else{
            res.status(404).json({
                message: "not found"
            })
        }
    })

}
//update carousel by id (put)
const updateCarouselById = (req, res)=>{
    // b1: thu thập dữ liệu từ request
   let body = req.body;
    let carouselId = req.params.carouselId;
    // b2: validate
    // Khi update không truyền đầy đủ các trường thì vẫn hợp lệ
    // Khi không truyền trường nào thì trường đó sẽ có giá trị là undefined
    if(!body.name){
        return res.status(400).json({
            status: "bad request",
            message: "name is required"
        })
    }
    if(!body.description){
        return res.status(400).json({
            status: "bad request",
            message: "description is required"
        })
    }
    
    if(!mongoose.Types.ObjectId.isValid(carouselId)){
        return res.status(400).json({
            status: "Bad request",
            message:"carouselId is not valid"
        })
    }
    // b3: gọi model thao tác với CSDL
    var carouselUpdate = {
        name : body.name,
        description : body.description,
        imageUrl: body.imageUrl
    }
    carouselModel.findByIdAndUpdate(carouselId, carouselUpdate, (err, data)=>{
        if(err){
            res.status(500).json({
                status: "internal server error",
                error: err.message
            })
        }
        if(data){
            return res.status(200).json({
                status: "update carousel successfuly",
                carousel: data
            })
       } else{
            return res.status(400).json({
                status: "not found"
            })
       }
    })
}
//delete
const deletearouselById = (req, res)=>{
    // b1: thu thập dữ liệu từ request
    let carouselId = req.params.carouselId;
    // b2: validate
    if(!mongoose.Types.ObjectId.isValid(carouselId)){
        return res.status(400).json({
            message: "carouselId is not valid"
        })
    }
    // b3: gọi model thao tác với CSDL
    carouselModel.findByIdAndDelete(carouselId, (err, data)=>{
        if(err){
            return res.status(500).json({
                status: "internal erver error",
                error: err.message
            })
        }
        if(data){
            return res.status(200).json({
                message: "delete carousel successfuly",
                carousel: data
            })
        }else{
            return res.status(204).json({
                message: "not found"
            })
        }
    })
}
// export module
module.exports = {createCarousel, getAllCarousel, getCarouselById, updateCarouselById, deletearouselById}
