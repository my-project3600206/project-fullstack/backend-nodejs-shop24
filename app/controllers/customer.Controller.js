const mongoose = require("mongoose");
//import Model
const customerModel = require("../models/customer.Model");

// create
const createCustomer = (req, res)=>{
    // b1: get data from request
    let body = req.body;
    // b2: validate
    if(!body.fullName){
        return res.status(400).json({
            status: "Bad Request",
            message : "fullName is required"
        })
    }
    
    if(!body.phone){
        return res.status(400).json({
            status: "Bad Request",
            message : "phone is required"
        })
    }
    if(!body.email){
        return res.status(400).json({
            status: "Bad Request",
            message : "email is required"
        })
    }
    if(!body.country){
        return res.status(400).json({
            status: "Bad Request",
            message : "country is required"
        })
    }
    if(!body.city){
        return res.status(400).json({
            status: "Bad Request",
            message : "city is required"
        })
    }
    if(!body.address){
        return res.status(400).json({
            status: "Bad Request",
            message : "address is required"
        })
    }
    
    // b3: xử lý và trả về dữ liệu
    let newCustomer = new customerModel({
        
        fullName : body.fullName,
        phone : body.phone,
        email: body.email,
        address : body.address,
        city : body.city,
        country : body.country

    });
    // thao tác với csdl
    customerModel.create(newCustomer,(err,data) =>{
        if(err){
            return res.status(500).json({
                message : `Internal server error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message :`Create New Customer Successfully !`,
                customer : data
            })
        }
    })

}
// get all product
const getAllCustomer = (req, res)=>{
    // b1: get data from request
    // b2: validate
    // b3: xử lý dữ liệu và trả về kết quả
    customerModel.find((err, data)=>{
        if(err){
            return res.status(500).json({
                status: "internal server error",
                error: err.message
            })
        }else{
            return res.status(200).json({
                message: "get all data successfuly",
                customer: data
            })
        }
    })
}
// get product by id
const getCustomerById = (req, res)=>{
    // b1: get data from request
    let customerId = req.params.customerId;
    // b2: validate
    if(!mongoose.Types.ObjectId.isValid(customerId)){
        return res.status(400).json({
            message: "customerId is not valid"
        })
    }
    
    // b3: xử lý và trả về dữ liệu
    
    customerModel.findById(customerId,(err,data) =>{
        if(err){
            return res.status(500).json({
                message : `Internal server error : ${err.message}`
            })
        }
        if(data){
            return res.status(200).json({
                message: "get customer by id successfuly",
                customer: data
            })
        }else{
            res.status(404).json({
                message: "not found"
            })
        }
    })

}
//update product by id (put)
const updateCustomerById = (req, res)=>{
    // b1: thu thập dữ liệu từ request
    let body = req.body;
    let customerId = req.params.customerId;
    // b2: validate
    // Khi update không truyền đầy đủ các trường thì vẫn hợp lệ
    // Khi không truyền trường nào thì trường đó sẽ có giá trị là undefined
    if(!body.fullName){
        return res.status(400).json({
            status: "Bad Request",
            message : "fullName is required"
        })
    }
    
    if(!body.phone){
        return res.status(400).json({
            status: "Bad Request",
            message : "phone is required"
        })
    }
    if(!body.email){
        return res.status(400).json({
            status: "Bad Request",
            message : "email is required"
        })
    }
    if(!body.address){
        return res.status(400).json({
            status: "Bad Request",
            message : "address is required"
        })
    }
    if(!body.city){
        return res.status(400).json({
            status: "Bad Request",
            message : "city is required"
        })
    }
    if(!body.country){
        return res.status(400).json({
            status: "Bad Request",
            message : "city is required"
        })
    }

    if(!mongoose.Types.ObjectId.isValid(customerId)){
        return res.status(400).json({
            status: "Bad request",
            message:"customerId is not valid"
        })
    }
    // b3: gọi model thao tác với CSDL
    var customerUpdate =  {
        fullName: body.fullName,
        phone: body.phone,
        email: body.email,
        address : body.address,
        city : body.city,
        country : body.country
    }
    customerModel.findByIdAndUpdate(customerId, customerUpdate, (err, data)=>{
        if(err){
            res.status(500).json({
                status: "internal server error",
                error: err.message
            })
        }
        if(data){
            return res.status(200).json({
                status: "update customer successfuly",
                customer: data
            })
       } else{
            return res.status(400).json({
                status: "not found"
            })
       }
    })
}
//delete
const deleteCustomerById = (req, res)=>{
    // b1: thu thập dữ liệu từ request
    let customerId = req.params.customerId;
    // b2: validate
    if(!mongoose.Types.ObjectId.isValid(customerId)){
        return res.status(400).json({
            message: "productId is not valid"
        })
    }
    // b3: gọi model thao tác với CSDL
    customerModel.findByIdAndDelete(customerId, (err, data)=>{
        if(err){
            return res.status(500).json({
                status: "internal erver error",
                error: err.message
            })
        }
        if(data){
            return res.status(200).json({
                message: "delete customer successfuly",
                customer: data
            })
        }else{
            return res.status(204).json({
                message: "not found"
            })
        }
    })
}
// export module
module.exports = {createCustomer, getAllCustomer, getCustomerById, updateCustomerById, deleteCustomerById}
