const mongoose = require("mongoose");
//import order Model
const orderModel = require("../models/order.Model");
// import customer model
const customerModel = require("../models/customer.Model");

// create orders of customer
const createOrderOfCustomer = (req, res) => {
    // b1: get data from request
    let body = req.body;
    let customerId = req.params.customerId;
    // b2: validate
    if (!Number(body.cost)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "cost is a number"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            message: "customerId is not valid"
        })
    }
    // b3: xử lý và trả về dữ liệu
    let newOrder = new orderModel({
        _id: mongoose.Types.ObjectId(),
        orderDate: body.orderDate,
        shippedDate: body.shippedDate,
        note: body.note,
        cost: body.cost
    });
    orderModel.create(newOrder, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: `Internal server error : ${err.message}`
            })
        } else {
            // Khi tạo order xong cần thêm id order mới vào mảng order của customer
            customerModel.findByIdAndUpdate(customerId, { $push: { orders: data._id } }, (err1, data1) => {
                if (err1) {
                    return res.status(500).json({
                        message: "Internal server error  !",
                        error: err1.message
                    })
                } else {
                    return res.status(201).json({
                        message: "Create Order Successfully !",
                        order: data
                    })
                }
            })
        }
    })
}
// get all order
const getAllOrder = (req, res) => {
    // b1: get data from request
    // b2: validate
    // b3: xử lý dữ liệu và trả về kết quả
    orderModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                status: "internal server error",
                error: err.message
            })
        } else {
            return res.status(200).json({
                message: "get all data successfuly",
                order: data
            })
        }
    })
}
//get all order of customer
const getAllOrderOfCustomer = (req, res) => {
    // b1: thu thập dữ liệu
    let customerId = req.params.customerId
    // b2: validate
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            message: "customerId is not valid"
        })
    }
    // b3: xử lý và trả về kết quả
    customerModel.findById(customerId)
        .populate("orders")
        .exec((err, data) => {
            if (err) {
                return res.status(500).json({
                    message: `internal server erorr: ${err.message}`
                })
            }
            if (data) {
                return res.status(200).json({
                    message: "get data successfuly",
                    order: data
                })
            } else {
                return res.status(404).json({
                    message: "not found"
                })
            }
        })
}
// get order by id
const getOrderById = (req, res) => {
    // b1: get data from request
    let orderId = req.params.orderId;
    // b2: validate
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            message: "orderId is not valid"
        })
    }

    // b3: xử lý và trả về dữ liệu

    orderModel.findById(orderId, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: `Internal server error : ${err.message}`
            })
        }
        if (data) {
            return res.status(200).json({
                message: "get order by id successfuly",
                order: data
            })
        } else {
            res.status(404).json({
                message: "not found"
            })
        }
    })

}
//update order by id (put)
const updateOrderById = (req, res) => {
    // b1: thu thập dữ liệu từ request
    let body = req.body;
    let orderId = req.params.orderId;
    // b2: validate
    // Khi update không truyền đầy đủ các trường thì vẫn hợp lệ
    // Khi không truyền trường nào thì trường đó sẽ có giá trị là undefined

    if (!body.orderDate) {
        return res.status(400).json({
            status: "Bad Request",
            message: "orderDate is required"
        })
    }

    if (!body.shippedDate) {
        return res.status(400).json({
            status: "Bad Request",
            message: "shippedDate is required"
        })
    }
    if (!body.note) {
        return res.status(400).json({
            status: "Bad Request",
            message: "note is required"
        })
    }
    if (!body.cost) {
        return res.status(400).json({
            status: "Bad Request",
            message: "cost is required"
        })
    }

    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "orderId is not valid"
        })
    }
    // b3: gọi model thao tác với CSDL
    var orderUpdate = {
        orderDate: body.orderDate,
        shippedDate: body.shippedDate,
        note: body.note,
        cost: body.cost,
    }
    orderModel.findByIdAndUpdate(orderId, orderUpdate, (err, data) => {
        if (err) {
            res.status(500).json({
                status: "internal server error",
                error: err.message
            })
        }
        if (data) {
            return res.status(200).json({
                status: "update order successfuly",
                order: data
            })
        } else {
            return res.status(400).json({
                status: "not found"
            })
        }
    })
}
//delete
const deleteOrderById = (req, res) => {
    // b1: thu thập dữ liệu từ request
    let orderId = req.params.orderId;
    let customerId = req.params.customerId;
    // b2: validate
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            message: "orderId is not valid"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            message: "customerId is not valid"
        })
    }
    // b3: gọi model thao tác với CSDL
    orderModel.findByIdAndDelete(orderId, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "internal erver error",
                error: err.message
            })
        }
        customerModel.findByIdAndUpdate(customerId,{
            $pull : {orders : data._id}
        },(err1,data1) =>{
            if(err1){
                return res.status(500).json({
                    message : `Internal server error !`
                })
            }else{
                return res.status(204).json({
                    message: `delete order success`,
                })
            }
        }
        )
    })
}
// export module
module.exports = { createOrderOfCustomer, getAllOrderOfCustomer, getAllOrder, getOrderById, updateOrderById, deleteOrderById }
