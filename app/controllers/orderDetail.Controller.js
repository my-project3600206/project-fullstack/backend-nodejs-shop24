const mongoose = require("mongoose");
//import order Model
const orderDetailModel = require("../models/orderDetail.Model");
// import order model
const orderModel = require("../models/order.Model");

// create orders of customer
const createOrderDetailOfOrder = (req, res) => {
    // b1: get data from request
    let body = req.body;
    let orderId = req.params.orderId;
    // b2: validate
    if (!Number(body.quantity)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "quantity is a number"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            message: "orderId is not valid"
        })
    }
    // b3: xử lý và trả về dữ liệu
    var newOrderDetail = new orderDetailModel({
        _id: mongoose.Types.ObjectId(),
        quantity: body.quantity,

    });
    orderDetailModel.create(newOrderDetail, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: `Internal server error : ${err.message}`
            })
        } else {
            // Khi tạo orderdetail xong cần thêm id orderDetail mới vào mảng orderDetail của order
            orderModel.findByIdAndUpdate(orderId, { $push: { orderDetails: data._id } }, (err1, data1) => {
                if (err1) {
                    return res.status(500).json({
                        message: "Internal server error  !",
                        error: err1.message
                    })
                } else {
                    return res.status(201).json({
                        message: "Create OrderDetail Successfully !",
                        product: data
                    })
                }
            })
        }
    })
}
// get all order
const getAllOrderDetail = (req, res) => {
    // b1: get data from request
    // b2: validate
    // b3: xử lý dữ liệu và trả về kết quả
    orderDetailModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                status: "internal server error",
                error: err.message
            })
        } else {
            return res.status(200).json({
                message: "get all data successfuly",
                orderDetail: data
            })
        }
    })
}
//get all order of customer
const getAllOrderDetailOfOrder = (req, res) => {
    // b1: thu thập dữ liệu
    let orderId = req.params.orderId
    // b2: validate
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            message: "customerId is not valid"
        })
    }
    // b3: xử lý và trả về kết quả
    orderModel.findById(orderId)
        .populate("product")
        .exec((err, data) => {
            if (err) {
                return res.status(500).json({
                    message: "internal server error",
                    error: err.message
                })
            } else {
                return res.status(201).json({
                    message: "get all orderDetail of order successfuly",
                    data: data.orders
                })
            }
        })
}
// get order by id
const getOrderDetailById = (req, res) => {
    // b1: get data from request
    let orderDetailId = req.params.orderDetailId;
    // b2: validate
    if (!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return res.status(400).json({
            message: "orderDetailId is not valid"
        })
    }

    // b3: xử lý và trả về dữ liệu

    orderDetailModel.findById(orderDetailId, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: `Internal server error : ${err.message}`
            })
        }
        if (data) {
            return res.status(200).json({
                message: "get orderDetailId by id successfuly",
                orderDetail: data
            })
        } else {
            res.status(404).json({
                message: "not found"
            })
        }
    })

}
//update order by id (put)
const updateOrderDetailById = (req, res) => {
    // b1: thu thập dữ liệu từ request
    let body = req.body;
    let orderDetailId = req.params.orderDetailId;
    // b2: validate
    // Khi update không truyền đầy đủ các trường thì vẫn hợp lệ
    // Khi không truyền trường nào thì trường đó sẽ có giá trị là undefined

    if (!Number(body.quantity)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "quantity is number"
        })
    }

    if (!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "orderDetailId is not valid"
        })
    }
    // b3: gọi model thao tác với CSDL
    var orderDetailUpdate = {
        quantity: body.quantity,

    }
    orderDetailModel.findByIdAndUpdate(orderDetailId, orderDetailUpdate, (err, data) => {
        if (err) {
            res.status(500).json({
                status: "internal server error",
                error: err.message
            })
        }
        if (data) {
            return res.status(200).json({
                status: "update orderDetail successfuly",
                order: data
            })
        } else {
            return res.status(400).json({
                status: "not found"
            })
        }
    })
}
//delete
const deleteOrderDetailById = (req, res) => {
    // b1: thu thập dữ liệu từ request
    let orderDetailId = req.params.orderDetailId;
    let orderId = req.params.orderId;
    // b2: validate
    if (!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return res.status(400).json({
            message: "orderDetailId is not valid"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            message: "orderId is not valid"
        })
    }
    // b3: gọi model thao tác với CSDL
    orderDetailModel.findByIdAndDelete(orderDetailId, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "internal erver error",
                error: err.message
            })
        }
        orderModel.findByIdAndUpdate(orderId,
            {
                $pull: { orderDetails: data._id }
            },
            (err1, data1) => {
                if (err1) {
                    return res.status(500).json({
                        message: `internal server err`,
                    })
                } else {
                    return res.status(204).json({
                        message: `delete orderDetail successfuly`,
                    })
                }
            })

    })
}
// export module
module.exports = { createOrderDetailOfOrder, getAllOrderDetail, getAllOrderDetailOfOrder, getOrderDetailById, updateOrderDetailById, deleteOrderDetailById }
