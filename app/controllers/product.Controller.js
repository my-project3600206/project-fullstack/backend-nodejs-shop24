const mongoose = require("mongoose");
//import Model
const productModel = require("../models/product.Model");


// create
const createProduct = (req, res) => {
    // b1: get data from request

    let body = req.body;
    // b2: validate
    if (!body.name) {
        return res.status(400).json({
            status: "Bad Request",
            message: "name is required"
        })
    }

    if (!body.description) {
        return res.status(400).json({
            status: "Bad Request",
            message: "description is required"
        })
    }
    if (!body.imageUrl) {
        return res.status(400).json({
            status: "Bad Request",
            message: "imageUrl is required"
        })
    }
    if (!Number(body.buyPrice)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "buyPrice is required"
        })
    }
    if (!Number(body.promotionPrice)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "promotionPrice is required"
        })
    }
    if (!Number(body.amount)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "amount is required"
        })
    }
    if (!body.brand) {
        return res.status(400).json({
            status: "Bad Request",
            message: "brand is required"
        })
    }
    if (!body.color) {
        return res.status(400).json({
            status: "Bad Request",
            message: "color is required"
        })
    }

    // b3: xử lý và trả về dữ liệu
    let newProduct = new productModel({
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description,
        type: body.type,
        imageUrl: body.imageUrl,
        buyPrice: body.buyPrice,
        promotionPrice: body.promotionPrice,
        amount: body.amount,
        brand: body.brand,
        color: body.color
    });

    productModel.create(newProduct, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: `Internal server error : ${err.message}`
            })
        } else {
            return res.status(201).json({
                message: `Create New product Successfully !`,
                data: data
            })
        }

    })

}
// get all product
const getAllProduct = (req, res) => {
    // b1: get data from request
    let { limit, page } = req.query;

    // b2: validate
    // b3: xử lý dữ liệu và trả về kết quả
    productModel.find().populate("type")
        .then((data) => {
            if (+limit > 0) {
                let newData = data.slice(0, +limit)
                return res.status(200).json({
                    message: "get all data successfuly",
                    product: newData
                })
            } else {
                return res.status(200).json({
                    message: "get all data successfuly",
                    product: data
                })
            }

        })
        .catch((err) => {
            return res.status(500).json({
                status: "internal server error",
                error: err.message
            })
        })
}
// get data brand and color
const getAllBrandAndColor = (req, res) => {
    productModel.aggregate([
        { $group: { _id: null, color: { $addToSet: "$color" }, brand: { $addToSet: "$brand" } } },
        { $project: { _id: 0, color: 1, brand: 1 } }
    ], (err, result) => {
        if (err) {
            return res.status(500).json({
                message: `internal servoer error: ${err.message}`
            })
        } else {
            const uniqueColor = result[0].color;
            const uniqueBrand = result[0].brand;
            const formattedResult = {
                color: uniqueColor,
                brand: uniqueBrand
            };
           
            return res.status(200).json({
                message: "get brand and color successfuly",
                data: formattedResult
            })
        }
    })
}
// get pagination
const getProductPagination = (req, res) => {
    // b1: get data from request
    let { limit, page } = req.query;

    // b2: validate
    // b3: xử lý dữ liệu và trả về kết quả
    productModel.find()
        .populate("type")
        .limit(limit)
        .skip(limit * (page - 1))
        .then((productList) => {
            productModel.countDocuments()
                .then((count) => {
                    let numberPage = Math.ceil(count / limit)
                    console.log("numberPage is:", numberPage)
                    return res.status(201).json({
                        message: "get pagination successfuly",
                        productList,
                        numberPage
                    })

                })
                .catch((err) => {
                    res.status(500).json({
                        message: `Internal sever error: ${err}`
                    })
                })

        })
        .catch((err) => {
            return res.status(500).json({
                status: "internal server error",
                error: err.message
            })
        })
}
// get product by filter
const getProductByFilter = (req, res) => {
    let filterConditionQuery = req.query;
    let brand = (filterConditionQuery?.brand === undefined)|| (!filterConditionQuery.brand) ? [] : filterConditionQuery.brand.split(",")
    let color = (filterConditionQuery?.color === undefined) || (!filterConditionQuery.color) ? [] : filterConditionQuery.color.split(",")
    let priceMin = !filterConditionQuery.min ? 0 : +filterConditionQuery.min
    let priceMax = !filterConditionQuery.max ? 10000 : +filterConditionQuery.max
    let page = !filterConditionQuery.page ? 1 : filterConditionQuery.page
    let limit = !filterConditionQuery.limit ? 8 : filterConditionQuery.limit
    let name =  filterConditionQuery.keyword

    console.log("name", filterConditionQuery.name)

    let filterCondition = {};
    if (brand.length > 0) {
        filterCondition.brand = {
            $in: brand
        }
    }
    if (color.length > 0) {
        filterCondition.color = {
            $in: color
        }
    }
    if (name !== undefined && name !== null) {
        filterCondition.name = {
            "$regex": name, "$options": "i"
        }
    }
    if ((priceMin !== undefined || priceMin !== NaN) && priceMin <= priceMax) {
        filterCondition.promotionPrice = {
            $gte: priceMin
        }
    }
    if ((priceMax !== undefined || priceMax !== NaN) && priceMax >= priceMin) {
        filterCondition.promotionPrice = {
            ...filterCondition.promotionPrice,
            $lte: priceMax
        }
    }
    productModel.find(filterCondition)
    .then((dataFilter)=>{
        let numberPage = Math.ceil(dataFilter.length / limit)
        console.log("numberPage is:", numberPage)

        productModel.find(filterCondition)
        .populate("type")
        .limit(limit)
        .skip(limit * (page - 1))
        .then((productList) => { 
            return res.status(201).json({
                message: "get productList successfuly",
                productList,
                numberPage
            })
               
        })
        .catch((err) => {
            return res.status(500).json({
                status: "internal server error",
                error: err.message
            })
        })

    })
    .catch((err)=>{
        return res.status(500).json({
            status: "internal server error",
            err: err.message
        })

    })
   


}
// get product by id
const getProductById = (req, res) => {
    // b1: get data from request
    let productId = req.params.productId;

    // b2: validate
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            message: "productId is not valid"
        })
    }

    // b3: xử lý và trả về dữ liệu

    productModel.findById(productId, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: `Internal server error : ${err.message}`
            })
        }
        if (data) {

            return res.status(200).json({
                message: "get product by id successfuly",
                product: data
            })
        } else {
            res.status(404).json({
                message: "not found"
            })
        }
    })

}
//update product by id (put)
const updateProductById = (req, res) => {
    // b1: thu thập dữ liệu từ request
    let body = req.body;
    let productId = req.params.productId;
    // b2: validate
    // Khi update không truyền đầy đủ các trường thì vẫn hợp lệ
    // Khi không truyền trường nào thì trường đó sẽ có giá trị là undefined
    if (!body.name) {
        return res.status(400).json({
            status: "Bad Request",
            message: "name is required"
        })
    }

    if (!body.description) {
        return res.status(400).json({
            status: "Bad Request",
            message: "description is required"
        })
    }
    if (!body.imageUrl) {
        return res.status(400).json({
            status: "Bad Request",
            message: "imageUrl is required"
        })
    }
    if (!body.brand) {
        return res.status(400).json({
            status: "Bad Request",
            message: "brand is required"
        })
    }
    if (!body.color) {
        return res.status(400).json({
            status: "Bad Request",
            message: "color is required"
        })
    }
    // s

    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "productId is not valid"
        })
    }
    // b3: gọi model thao tác với CSDL
    var productUpdate = {
        name: body.name,
        description: body.description,
        type: body.type,
        imageUrl: body.imageUrl,
        buyPrice: body.buyPrice,
        promotionPrice: body.promotionPrice,
        amount: body.amount,
        brand: body.brand,
        color: body.color
    }
    productModel.findByIdAndUpdate(productId, productUpdate, (err, data) => {
        if (err) {
            res.status(500).json({
                status: "internal server error",
                error: err.message
            })
        }
        if (data) {
            return res.status(200).json({
                status: "update product successfuly",
                product: data
            })
        } else {
            return res.status(400).json({
                status: "not found"
            })
        }
    })
}
//delete
const deleteProductById = (req, res) => {
    // b1: thu thập dữ liệu từ request
    let productId = req.params.productId;
    // b2: validate
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            message: "productId is not valid"
        })
    }
    // b3: gọi model thao tác với CSDL db
    productModel.findByIdAndDelete(productId, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "internal erver error",
                error: err.message
            })
        }
        if (data) {
            return res.status(200).json({
                message: "delete prduct successfuly",
                product: data
            })
        } else {
            return res.status(204).json({
                message: "not found"
            })
        }
    })
}
// export module
module.exports = {
    createProduct,
    getAllProduct,
    getProductById,
    updateProductById,
    deleteProductById,
    getProductPagination,
    getProductByFilter,
    getAllBrandAndColor
}
