const mongoose = require("mongoose");
//import Model
const productTypeModel = require("../models/productType.Model");


// create
const createProductType = (req, res)=>{
    // b1: get data from request
    const productTypeId = req.params.productTypeId
    let body = req.body;
    
    // b2: validate
   
    if(!body.name){
        return res.status(400).json({
            status: "Bad Request",
            message : "name is required"
        })
    }
    
    if(!body.description){
        return res.status(400).json({
            status: "Bad Request",
            message : "description is required"
        })
    }
    
    // b3: xử lý và trả về dữ liệu
    var newProductType = new productTypeModel({
        _id:mongoose.Types.ObjectId(),
        name : body.name,
        description : body.description
    });

    productTypeModel.create(newProductType,(err,data) =>{
        if(err){
            return res.status(500).json({
                message : `Internal server error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message :`Create New productType Successfully !`,
                productType : data
            })
        }
       
        
    })

}
// get all productType
const getAllProductType = (req, res)=>{
    // b1: get data from request
   
    // b2: validate
    // b3: xử lý dữ liệu và trả về kết quả
    productTypeModel.find((err, data)=>{
        if(err){
            return res.status(500).json({
                status: "internal server error",
                error: err.message
            })
        }else{
            return res.status(200).json({
                message: "get all data successfuly",
                productType: data
            })
        }
    })
}

// get productType by id
const getProductTypeById = (req, res)=>{
    // b1: get data from request
    let productTypeId = req.params.productTypeId;
    // b2: validate
    if(!mongoose.Types.ObjectId.isValid(productTypeId)){
        return res.status(400).json({
            message: "productId is not valid"
        })
    }
    
    // b3: xử lý và trả về dữ liệu
    
    productTypeModel.findById(productTypeId,(err,data) =>{
        if(err){
            return res.status(500).json({
                message : `Internal server error : ${err.message}`
            })
        }
        if(data){
            return res.status(200).json({
                message: "get productType by id successfuly",
                productType: data
            })
        }else{
            res.status(404).json({
                message: "not found"
            })
        }
    })

}
//update productType by id (put)
const updateProductTypeById = (req, res)=>{
    // b1: thu thập dữ liệu từ request
   let body = req.body;
    let productTypeId = req.params.productTypeId;
    // b2: validate
    // Khi update không truyền đầy đủ các trường thì vẫn hợp lệ
    // Khi không truyền trường nào thì trường đó sẽ có giá trị là undefined
    if(!body.name){
        return res.status(400).json({
            status: "bad request",
            message: "name is required"
        })
    }
    if(!body.description){
        return res.status(400).json({
            status: "bad request",
            message: "description is required"
        })
    }
    
    if(!mongoose.Types.ObjectId.isValid(productTypeId)){
        return res.status(400).json({
            status: "Bad request",
            message:"productTypeId is not valid"
        })
    }
    // b3: gọi model thao tác với CSDL
    var productTypeUpdate = {
        name: body.name,
        description: body.description
    }
    productTypeModel.findByIdAndUpdate(productTypeId, productTypeUpdate, (err, data)=>{
        if(err){
            res.status(500).json({
                status: "internal server error",
                error: err.message
            })
        }
        if(data){
            return res.status(200).json({
                status: "create successfuly",
                productType: data
            })
       } else{
            return res.status(400).json({
                status: "not found"
            })
       }
    })
}
//delete
const deleteProductTypeById = (req, res)=>{
    // b1: thu thập dữ liệu từ request
    let productTypeId = req.params.productTypeId;
    // b2: validate
    if(!mongoose.Types.ObjectId.isValid(productTypeId)){
        return res.status(400).json({
            message: "productTypeId is not valid"
        })
    }
    // b3: gọi model thao tác với CSDL
    productTypeModel.findByIdAndDelete(productTypeId, (err, data)=>{
        if(err){
            return res.status(500).json({
                status: "internal erver error",
                error: err.message
            })
        }
        if(data){
            return res.status(200).json({
                message: "delete successfuly",
                productType: data
            })
        }else{
            return res.status(204).json({
                message: "not found"
            })
        }
    })
}
// export module
module.exports = {createProductType, getAllProductType, getProductTypeById, updateProductTypeById, deleteProductTypeById}
