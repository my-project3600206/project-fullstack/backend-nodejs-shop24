const mongoose = require('mongoose');
const voucherModel = require("../models/voucher.model");

// create voucher
const voucherCreate = (req, res)=>{
    //b1: get data from request
    
    let body = req.body
    //b2: validate
   //b3: handle and return data
   var newVoucher = new voucherModel({
    _id: mongoose.Types.ObjectId(),
    discount: body.discount
   });
   voucherModel.create(newVoucher)
   .then((data)=>{
        res.status(201).json({
            message: "create voucher successfully",
            voucher: data
        })
   })
   .catch((err)=>{
        res.status(500).json({
            message: `internal server error: ${err}`
        })
   })
}

// get all voucher
const voucherGetAll = (req, res)=>{
    //b1: get data from request
    //b2: validate
   //b3: handle and return data
  
   voucherModel.find()
   .then((data)=>{
        res.status(201).json({
            message: "get all voucher successfully",
            voucher: data
        })
   })
   .catch((err)=>{
        res.status(500).json({
            message: `internal server error: ${err}`
        })
   })
}
// get voucher by code
const voucherGetByCode = (req, res)=>{
    //b1: get data from request
    const code = req.params.code
    //b2: validate
   //b3: handle and return data
  
   voucherModel.findOne({code:code})
   .then((data)=>{
        res.status(201).json({
            message: "get voucher by code successfully",
            voucher: data
        })
   })
   .catch((err)=>{
        res.status(500).json({
            message: `internal server error: ${err}`
        })
   })
}
// get voucher by id
const voucherGetById = (req, res)=>{
    //b1: get data from request
    const idVoucher = req.params.idVoucher
    //b2: validate
    if (!mongoose.Types.ObjectId.isValid(idVoucher)) {
        return res.status(400).json({
            message: "idVoucher is not valid"
        })
    }
   //b3: handle and return data
  
   voucherModel.findById(idVoucher)
   .then((data)=>{
        res.status(201).json({
            message: "get voucher by idVoucher successfully",
            voucher: data
        })
   })
   .catch((err)=>{
        res.status(500).json({
            message: `internal server error: ${err}`
        })
   })
}
// update voucher by id
const voucherUpdateById = (req, res)=>{
    //b1: get data from request
    const idVoucher = req.params.idVoucher
    let body = req.body
    //b2: validate
    if (!mongoose.Types.ObjectId.isValid(idVoucher)) {
        return res.status(400).json({
            message: "idVoucher is not valid"
        })
    }
    if (!body.discount) {
        return res.status(400).json({
            status: "Bad Request",
            message: "discount is required"
        })
    }
   //b3: handle and return data
   let newVoucher = new voucherModel({
    discount: body.discount
   });
   voucherModel.findByIdAndUpdate(idVoucher,newVoucher)
   .then((data)=>{
        res.status(201).json({
            message: "update voucher by idVoucher successfully",
            voucher: data
        })
   })
   .catch((err)=>{
        res.status(500).json({
            message: `internal server error: ${err}`
        })
   })
}
// delete voucher by id
const voucherDeleteById = (req, res)=>{
    //b1: get data from request
    const idVoucher = req.params.idVoucher
    //b2: validate
    if (!mongoose.Types.ObjectId.isValid(idVoucher)) {
        return res.status(400).json({
            message: "idVoucher is not valid"
        })
    }
   
   //b3: handle and return data
  
   voucherModel.findByIdAndDelete(idVoucher)
   .then((data)=>{
        res.status(201).json({
            message: "delete voucher by idVoucher successfully",
            voucher: data
        })
   })
   .catch((err)=>{
        res.status(500).json({
            message: `internal server error: ${err}`
        })
   })
}
module.exports = {
    voucherCreate,
    voucherGetAll,
    voucherGetByCode,
    voucherGetById,
    voucherUpdateById,
    voucherDeleteById
}