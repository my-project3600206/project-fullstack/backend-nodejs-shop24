// khai bao thu vien express 
const mongoose = require('mongoose');
//khai bao Schema
const Schema = mongoose.Schema;
// khởi tạo đối tượng schema bao gồm các thuộc tính của collection trong mongoose
const carouselSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    imageUrl: {
        type: String,
        required: true
    }
}, {timestamps : true
})

// export module
module.exports = mongoose.model("carousel", carouselSchema);