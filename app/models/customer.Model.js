// khai bao thu vien express 
const mongoose = require('mongoose');
//khai bao Schema
const Schema = mongoose.Schema;
// khởi tạo đối tượng schema bao gồm các thuộc tính của collection trong mongoose
const customerSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    fullName: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true,
        unique: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    address: {
        type: String,
        default: ""
    },
    city: {
        type: String,
        default: ""
    },
    country: {
        type: String,
        default: ""
    },
    orders: [
        {
        type: mongoose.Types.ObjectId,
        ref: "order",
        }
    ]
}, {timestamps : true
})

// export module
module.exports = mongoose.model("customer", customerSchema);