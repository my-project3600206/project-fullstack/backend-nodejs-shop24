// khai bao thu vien express 
const mongoose = require('mongoose');
//khai bao Schema
const Schema = mongoose.Schema;
// khởi tạo đối tượng schema bao gồm các thuộc tính của collection trong mongoose
const orderSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    orderDate: {
        type: Date,
        default: Date.now()
    },
    shippedDate: {
        type: Date,
    },
    note: {
        type: String,
    
    },
    orderDetails: [
        {
        type: mongoose.Types.ObjectId,
        ref: "OrderDetail"
        }
    ],
    cost: {
        type: Number,
        default: 0
    },
}, {timestamps : true
})

// export module
module.exports = mongoose.model("order", orderSchema);