// khai bao thu vien express 
const mongoose = require('mongoose');
//khai bao Schema
const Schema = mongoose.Schema;
// khởi tạo đối tượng schema bao gồm các thuộc tính của collection trong mongoose
const orderDetailSchema = new Schema({
    //1234
    _id: mongoose.Types.ObjectId,
    
    product:[
        {
            type:  mongoose.Types.ObjectId,
            ref: "product"
        }
    ],
        
    quantity: {
        type: Number,
        default: 0
    
    }

}, {timestamps : true
})

// export module
module.exports = mongoose.model("OrderDetail", orderDetailSchema);