// khai bao thu vien express 
const mongoose = require('mongoose');
//khai bao Schema
const Schema = mongoose.Schema;
// khởi tạo đối tượng schema bao gồm các thuộc tính của collection trong mongoose
const productSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    name: {
        type: String,
        unique: true,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    type: 
        {
        type: mongoose.Types.ObjectId,
        ref: "productType",
        required: true
        }
    ,
    imageUrl: [{
        type: String,
        required: true
    }],
    buyPrice: {
        type: Number,
        required: true
    },
    promotionPrice: {
        type: Number,
        required: true
    },
    amount: {
        type: Number,
        default: 0,
        required: true
    },
    brand : {
        type: String,
        required: true
    },
    color: {
        type: String,
        required: true
    }
}, {timestamps : true
})

// export module
module.exports = mongoose.model("product", productSchema);