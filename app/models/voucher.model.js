
const randtoken = require('rand-token');
const { default: mongoose } = require('mongoose');
const Schema = mongoose.Schema;
const voucher= new Schema ({
    _id: mongoose.Types.ObjectId,
    code: {
        type: String,
        unique: true,
        default: ()=>{
            return  randtoken.generate(8);
        }
    },
    discount: {
        type: Number,
        require: true
    },
},{
    timestamps: true
})
module.exports = mongoose.model("Voucher",voucher)