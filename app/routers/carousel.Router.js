// khai bao express
const express = require("express");
const { createCarousel, getAllCarousel, getCarouselById, updateCarouselById, deletearouselById } = require("../controllers/carousel.controller");

// khai bao router
const carouselRouter = express.Router();
// import controller

// create (post)
carouselRouter.post("/carousel", createCarousel);
// get all (get)
carouselRouter.get("/carousel", getAllCarousel);
// get all by id (get)
carouselRouter.get("/carousel/:carouselId", getCarouselById);
// update by id (put)
carouselRouter.put("/carousel/:carouselId", updateCarouselById);
// delete (delete)
carouselRouter.delete("/carousel/:carouselId", deletearouselById);


//export module
module.exports = {carouselRouter}