
// khai bao express
const express = require("express");

// khai bao router
const customerRouter = express.Router();
// import controller
const customerController = require("../controllers/customer.Controller");

// create (post)
customerRouter.post("/customers", customerController.createCustomer);
// get all (get)
customerRouter.get("/customers", customerController.getAllCustomer);
// get all by id (get)
customerRouter.get("/customers/:customerId", customerController.getCustomerById);
// update by id (put)
customerRouter.put("/customers/:customerId", customerController.updateCustomerById);
// delete (delete)
customerRouter.delete("/customers/:customerId", customerController.deleteCustomerById);



//export module
module.exports = {customerRouter}