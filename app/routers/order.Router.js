
// khai bao express
const express = require("express");

// khai bao router
const orderRouter = express.Router();
// import controller
const orderController = require("../controllers/order.Controller");

// create order of customer (post)
orderRouter.post("/customers/:customerId/orders", orderController.createOrderOfCustomer);
// get all order of customer
orderRouter.get("/customers/:customerId/orders", orderController.getAllOrderOfCustomer);
// get all (get)
orderRouter.get("/orders", orderController.getAllOrder);
// get all by id (get)
orderRouter.get("/orders/:orderId", orderController.getOrderById);
// update by id (put)
orderRouter.put("/orders/:orderId", orderController.updateOrderById);
// delete (delete)
orderRouter.delete("/customers/:customerId/orders/:orderId", orderController.deleteOrderById);



//export module
module.exports = {orderRouter}