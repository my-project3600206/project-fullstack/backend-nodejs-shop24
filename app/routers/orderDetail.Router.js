
// khai bao express
const express = require("express");

// khai bao router
const orderDetailRouter = express.Router();
// import controller
const orderDetailController = require("../controllers/orderDetail.Controller");

// create orderDetail of orders (post)
orderDetailRouter.post("/orders/:orderId/orderDetails", orderDetailController.createOrderDetailOfOrder);
// get all orderDetail of orders
orderDetailRouter.get("/orders/:orderId/orderDetails", orderDetailController.getAllOrderDetailOfOrder);
// get all (get)
orderDetailRouter.get("/orderDetails", orderDetailController.getAllOrderDetail);
// get all by id (get)
orderDetailRouter.get("/orderDetails/:orderDetailId", orderDetailController.getOrderDetailById);
// update by id (put)
orderDetailRouter.put("/orderDetails/:orderDetailId", orderDetailController.updateOrderDetailById);
// delete (delete)
orderDetailRouter.delete("/orders/:orderId/orderDetails/:orderDetailId", orderDetailController.deleteOrderDetailById);



//export module
module.exports = {orderDetailRouter}