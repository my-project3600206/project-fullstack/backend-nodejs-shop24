
// khai bao express
const express = require("express");

// khai bao router
const productRouter = express.Router();
// import controller
const productController = require("../controllers/product.Controller");

// create (post)
productRouter.post("/products", productController.createProduct);
// get all (get)
productRouter.get("/products", productController.getAllProduct);
// get data brand and color
productRouter.get("/products/brands/colors", productController.getAllBrandAndColor);
// get product pagination (get)
productRouter.get("/products/pagination", productController.getProductPagination);
//get product by filter
productRouter.get("/products/filters", productController.getProductByFilter);
// get all by id (get)
productRouter.get("/products/:productId", productController.getProductById);
// update by id (put)
productRouter.put("/products/:productId", productController.updateProductById);
// delete (delete)
productRouter.delete("/products/:productId", productController.deleteProductById);



//export module
module.exports = {productRouter}