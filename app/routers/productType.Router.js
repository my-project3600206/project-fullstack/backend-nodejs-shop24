// khai bao express
const express = require("express");

// khai bao router
const productTypeRouter = express.Router();
// import controller
const productTypeController = require("../controllers/productType.Controller");

// create (post)
productTypeRouter.post("/productType", productTypeController.createProductType);
// get all (get)
productTypeRouter.get("/productType", productTypeController.getAllProductType);
// get all by id (get)
productTypeRouter.get("/productType/:productTypeId", productTypeController.getProductTypeById);
// update by id (put)
productTypeRouter.put("/productType/:productTypeId", productTypeController.updateProductTypeById);
// delete (delete)
productTypeRouter.delete("/productType/:productTypeId", productTypeController.deleteProductTypeById);
//Get all productType of product




//export module
module.exports = {productTypeRouter}