// khai bao express
const express = require("express");

// khai bao router
const voucherRouter = express.Router();
// import controller
const voucherController = require("../controllers/voucher.Controller");

// create (post)
voucherRouter.post("/voucher", voucherController.voucherCreate);
// get all (get)
voucherRouter.get("/voucher", voucherController.voucherGetAll);
// get all by code (get)
voucherRouter.get("/voucher/code/:code", voucherController.voucherGetByCode);
// get by id (get)
voucherRouter.get("/voucher/:idVoucher", voucherController.voucherGetById);
//update voucher
voucherRouter.put("/voucher/:idVoucher", voucherController.voucherUpdateById);
// delete (delete)
voucherRouter.delete("/voucher/:idVoucher", voucherController.voucherDeleteById);

//export module
module.exports = {voucherRouter}