//import express
const express = require("express");

// import mongoose
const mongoose = require("mongoose");
// import cors
const cors = require("cors");
// import router
const { productTypeRouter } = require("./app/routers/productType.Router");
const { productRouter } = require("./app/routers/product.Router");
const { customerRouter } = require("./app/routers/customer.Router");
const { orderRouter } = require("./app/routers/order.Router");
const { orderDetailRouter } = require("./app/routers/orderDetail.Router");
const { carouselRouter } = require("./app/routers/carousel.Router");
const { voucherRouter } = require("./app/routers/voucher.Router");
// app initialization (khoi tao app)
const app = express();
// khai bao cong
const port = 8000;
// khai báo để đọc được json
app.use(express.json());
app.use(cors({
    origin: "http://localhost:3000"

}))
// connect mongoosedb version 6.x
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Shop24h",(error) =>{
    if(error) throw error;
    console.log("Connect Successfully !");
});


// khai bao API router
app.use(productTypeRouter);
app.use(productRouter);
app.use(customerRouter);
app.use(orderRouter);
app.use(orderDetailRouter);
app.use(carouselRouter);
app.use(voucherRouter);

// run app
app.listen(port, ()=>{
    console.log(`app running on port ${port}`)
})